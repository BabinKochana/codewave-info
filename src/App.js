import React, { Component } from 'react';
import './App.css';
import "react-table/react-table.css";
import FirstComponent from "./comp/FirstComponent";
import SecondComponent from "./comp/SecondComponent";
import PubNubReact from 'pubnub-react';
import Tabletop from 'tabletop';
import Fullscreen from "react-full-screen";
import {
  CarouselProvider,
  Slide,
  Slider
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import logo from "./assets/img/logo.png";
const moment = require('moment');
let fullIcon = require('./assets/icon/full _screen.svg');
class App extends Component {
  constructor() {
    super();

    this.pubnub = new PubNubReact({
      publishKey: "pub-c-ea73ff81-8ade-42b4-b9bd-8444c019046b",
      subscribeKey: "sub-c-2af33160-014a-11ea-af9f-8e6316f48a38"
    });
    this.pubnub.init(this);

    this.state = {
      projectArray: [],
      column: [],
      userArray: [],
      salesArray: [],
      isFull: false,
      isCompany: true,
      time: moment().format('DD MMMM hh:mm a')
    }
    this.handleTime = this.handleTime.bind(this);
  }

  componentDidMount() {
    this.getExcelData();
    this.pubNubInit();
    this.handleTime();
  }

  pubNubInit() {
    this.pubnub.subscribe({
      channels: ['excelTrigger']
    });

    this.pubnub.addListener({
      message: (message) => {
        this.getExcelData();
      }
    });
  }

  loaderOnOff(on) {
    let x = document.getElementById("loader");
    if (on) {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  goFull = () => {
    this.setState({ isFull: true });
  }

  componentWillUnmount() {
    this.pubnub.unsubscribe({
      channels: ['excelTrigger']
    });
  }

  getExcelData() {
    Tabletop.init({
      key: 'https://docs.google.com/spreadsheets/d/14GUOyBE1sCMCdBlioG7nAKUTpDEGQJ3TZksS9o89w00/edit',
      callback: excelData => {
        console.log(excelData, 'sheet_data');
        this.setState({
          projectArray: excelData.project_detail.elements,
          salesArray: excelData.sales_detail.elements,
          userArray: excelData.employee_detail.elements
        }, function () {
          this.loaderOnOff(false);
        });
      }
    });
  }

  handleEnd() {
    this.setState({
      isCompany: !this.state.isCompany,
    })
    // need to know .current slide index, 
    // otherwise the user might not have actually advanced the slider
  }

  handleTime() {
    setInterval(timer.bind(this), 60000)
    function timer() {
      this.setState({
        time: moment().format('DD MMMM hh:mm a')
      })
    }
  }

  render() {
    const { time } = this.state;
    return (
      <div>
        <button onClick={this.goFull} className="fullscreen-btn">
          <img alt="icon" src={fullIcon} />
        </button>
        <Fullscreen
          enabled={this.state.isFull}
          onChange={isFull => this.setState({ isFull })}
        >
          <div className={this.state.isCompany ?"company-info-div project-nv-color":"company-info-div sales-nv-color"}>
            <div className="col-md-4 btm-bar-align">
              {time}
            </div>
            <div className="col-md-4 btm-bar-align name-align">
              {this.state.isCompany ? "Project Dashboard" : "Sales Dashboard"}
            </div>
            <div className="col-md-4 logo-div btm-bar-align-logo">
              {/* <img alt="img" src="https://res.cloudinary.com/codewave-technologies/image/upload/c_scale,w_140/v1566712787/codewave-logo-2x_tl2pvw.png" /> */}
              {/* <img alt="logo" src="https://res.cloudinary.com/dw2lyv8si/image/upload/c_scale,h_23/v1581606877/timely.png"></img> */}
              <img alt="logo" src={logo}></img>
            </div>
          </div>
          <div className="carousel-div">
            <CarouselProvider totalSlides={2}
              interval={60000}
              naturalSlideWidth={1000}
              naturalSlideHeight={1000}
              isPlaying={true}
            >
              <Slider onTransitionEnd={this.handleEnd.bind(this)}>
                <Slide index={0}>
                  <FirstComponent data={this.state.projectArray} users={this.state.userArray} ></FirstComponent>
                </Slide>
                <Slide index={1}>
                  <SecondComponent data={this.state.salesArray} project={this.state.projectArray} users={this.state.userArray} ></SecondComponent>
                </Slide>
              </Slider>
            </CarouselProvider>
          </div>
          
        </Fullscreen>  
      </div>
    );
  }
}

export default App;
