import React, { Component } from 'react';
import SalesBarComponent from './blocks/main/sales-bar';
import ReadyToBoardBlockSalesComponent from './blocks/main/ready-to-board-block-sales';
// import VirtualDndBlockComponent from './blocks/main/virtual-dnd-block';
class SecondComponent extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      users: [],
      project: []
    }
  }

  componentDidMount() {
    this.setState({
      data: this.props.data,
      users: this.props.users,
      project: this.props.project
    });
  }

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps) !== JSON.stringify(this.props)) {
      console.log('babin')
      this.setState({
        data: nextProps.data,
        users: nextProps.users,
        project: nextProps.project
      });
    }
  }

  render() {
    const { data, users, project } = this.state;
    return (
      <div className="maindiv">
        <div className="subdiv">
          <div className="row no-gutters">
            <div className="col-md-12 no-gutters">
              <SalesBarComponent data={data} users={users} />
            </div>
            {/* <div className="col-md-2 no-gutters">
              <ReadyToBoardBlockSalesComponent users={users} project={project} />
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}

export default SecondComponent;
