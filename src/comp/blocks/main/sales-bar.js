import React, { Component } from 'react';
//import { Fade } from 'react-slideshow-image';
var classNames = require('classnames');
let localAnd = require('../../../assets/img/android.png');
let localIOS = require('../../../assets/img/reactnative.png');
let localWeb = require('../../../assets/img/web2.png');
const $ = window.$;
// const properties = {
//     duration: 5000,
//     transitionDuration: 1000,
//     infinite: true,
//     arrows: false,
//     onChange: (oldIndex, newIndex) => {
//         console.log(`slide transition from ${oldIndex} to ${newIndex}`);
//     }
// }

function isOdd(n) {
    return Math.abs(n % 2) === 1;
}

class SalesBarComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            users: [],
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.data,
            users: this.props.users
        });
        this.roundRobinScrollInit();
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps) !== JSON.stringify(this.props)) {
            this.setState({
                data: nextProps.data,
                users: nextProps.users
            });
        }
    }

    getPmMemberObj(team, userArry) {
        let teamObj = {};
        if (team) {
            let teamMen = userArry.filter((o) => Number(o.USER_ID) === Number(team));
            if (teamMen.length) {
                teamObj = teamMen[0];
            }
            return teamObj;
        } else {
            return null;
        }
    }

    getTeamMemberObj(team, userArry) {
        if (team) {
            let teamArr = team.split(',');
            let teamObj = [];
            if (teamArr.length) {
                // teamArr.map((obj) => {
                //     let teamMen = userArry.filter((o) => Number(o.USER_ID) === Number(obj));
                //     if (teamMen.length) {
                //         teamObj.push(teamMen[0])
                //     }
                // })
                for (let i = 0; i < teamArr.length; i++) {
                    let teamMen = userArry.filter((o) => Number(o.USER_ID) === Number(teamArr[i]));
                    if (teamMen.length) {
                        teamObj.push(teamMen[0])
                    }
                }
                return teamObj;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    spiltArray(a) {
        var arrays = [], size = 5;
        while (a.length > 0)
            arrays.push(a.splice(0, size));
        return arrays;
    }

    fadeSliderUi(teamMemberslist) {
        if (teamMemberslist.length) {
            let splitTeamMember = this.spiltArray(teamMemberslist);
            let memView = splitTeamMember.map((mO) => {
                return (<div>
                    {
                        mO.map((mem) => {
                            let iconColor = classNames({
                                'fa': true,
                                'fa-lock': true,
                                'user-present': mem.Available === 'yes',
                                'user-absent': mem.Available === 'no',
                                'user-wfh': mem.Available === 'wfh',
                            });
                            return (
                                <div className="tmdiv-img">
                                    {
                                        mem.Profile_Image ?
                                            <div>
                                                <img alt="icon" className="profile-img-tm" src={this.imgUrlMake(mem.Profile_Image)} />
                                                <i className={iconColor}></i>
                                            </div>
                                            : ''

                                    }
                                </div>
                            )
                        })
                    }
                </div>)
            });
            return memView;
        } else {
            return '';
        }
    }

    roundRobinScrollInit() {
        setTimeout(function () {
            setInterval(function () {
                $('.sb-vertical-inner').first()
                    .animate({ 'margin-top': '-=8.5vh' }, "slow", function () {
                        $(this).appendTo('.sb-vertical-scroll').css('margin-top', '');
                    });
            }, 4000);
        }, 3000)
    }

    imgUrlMake(url) {
        return url;
        // console.log(url)
        // let dummylink = 'https://drive.google.com/open?id=1G20Nb_LhZYfgrcwiEoNXhrRgoWaFTcU5'
        // if (url) {
        //     //return url.replace('/open', '/uc');
        //     return url;
        // } else {
        //     return dummylink.replace('/open', '/uc');;
        // }
    }

    isTimeLineDelayed(timelineTaken, timelineGiven) {
        let tlT = Number(timelineTaken);
        let tlG = Number(timelineGiven);
        if (tlT > tlG) {
            return true;
        } else {
            return false;
        }
    }

    platformSec(platformStr, platform) {
        return !platformStr.includes(platform)
    }

    render() {
        const { data, users } = this.state;
        return (
            <div>
                <div className="projectdiv">
                    <div className="row no-gutters">
                        <div className="col-md-3 col-md-custom-3 no-gutters">
                            <div className="heading">
                                <p>PROJECT</p>
                            </div>
                        </div>
                        <div className="col-md-1 col-md-custom-1 no-gutters">
                            <div className=" heading">
                                <p>STATUS</p>
                            </div>
                        </div>
                        <div className="col-md-1 col-md-custom-1 no-gutters">
                            <div className=" heading text-nv-center">
                                <p>BA</p>
                            </div>
                        </div>
                        <div className="col-md-2 no-gutters">
                            <div className="pmlist heading ">
                                <p>PROJECT TYPE</p>
                            </div>
                        </div>
                        <div className="col-md-2 no-gutters">
                            <div className="teammemberslist heading text-nv-center">
                                <p>PLATFORM</p>
                            </div>
                        </div>
                        <div className="col-md-3 col-md-custom-3 no-gutters">
                            <div className="teammemberslist heading">
                                <p>COMMENTS</p>
                            </div>
                        </div>
                    </div>
                    <div className="row no-gutters sb-vertical-scroll">
                        {
                            data.map((obj, i) => {
                                var projectClassMain = classNames({
                                    'projectlist': true,
                                    'project-good-odd': obj.PROJECT_STATUS === 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS === 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS === 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS === 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS === 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS === 'mid' && !isOdd(i)
                                });
                                var projectInner = classNames({
                                    'project-inner-div': true,
                                    'bor-lt1': obj.PROJECT_STATUS === 'good',
                                    'bor-lt2': obj.PROJECT_STATUS === 'bad',
                                    'bor-lt3': obj.PROJECT_STATUS === 'mid',
                                });
                                var timeLineDivClass = classNames({
                                    'timelinelist': true,
                                    'project-good-odd': obj.PROJECT_STATUS === 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS === 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS === 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS === 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS === 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS === 'mid' && !isOdd(i)
                                });
                                var pmClass = classNames({
                                    'pmimg': true,
                                    'project-good-odd': obj.PROJECT_STATUS === 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS === 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS === 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS === 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS === 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS === 'mid' && !isOdd(i)
                                });
                                var pltClass = classNames({
                                    'pltdiv': true,
                                    'project-good-odd': obj.PROJECT_STATUS === 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS === 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS === 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS === 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS === 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS === 'mid' && !isOdd(i)
                                });
                                var plaformClassA = classNames({
                                    plt_hidden: this.platformSec(obj.PLATFORM, 'A')
                                })
                                var plaformClassI = classNames({
                                    plt_hidden: this.platformSec(obj.PLATFORM, 'I')
                                })
                                var plaformClassW = classNames({
                                    plt_hidden: this.platformSec(obj.PLATFORM, 'W'),
                                    plt_img_rm_mar: true
                                });
                                var pmInfo = this.getPmMemberObj(obj['BA'], users);
                                let pmIconColor = classNames({
                                    'fa': true,
                                    'fa-lock': true,
                                    'user-present': pmInfo ? pmInfo.Available === 'yes' : false,
                                    'user-absent': pmInfo ? pmInfo.Available === 'no' : false,
                                    'user-wfh': pmInfo ? pmInfo.Available === 'wfh' : false,
                                });
                                return (
                                    <div className="sb-vertical-inner">
                                        <div className="col-md-3 col-md-custom-3 no-gutters">
                                            <div className={projectClassMain}>
                                                <div className={projectInner}>
                                                    <p>{obj.PROJECT}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-1 col-md-custom-1 no-gutters">
                                            <div className={timeLineDivClass}>
                                                <p className="p-padding3vh">{obj.STATUS}</p>
                                            </div>
                                        </div>
                                        <div className="col-md-1 col-md-custom-1 no-gutters">
                                            <div className={pmClass+" text-nv-center"}>
                                                {
                                                    pmInfo ?
                                                        <div className="pmimg_img">
                                                            <img alt="icon" className="profile-img" src={this.imgUrlMake(pmInfo.Profile_Image)} />
                                                            <i className={pmIconColor}></i>
                                                        </div> : ''
                                                }
                                            </div>
                                        </div>
                                        <div className="col-md-2 no-gutters">
                                            <div className={pmClass}>
                                                <p className="p-padding3vh">{obj.PROJECT_TYPE}</p>
                                            </div>
                                        </div>
                                        <div className="col-md-2 no-gutters">
                                            <div className={pltClass}>
                                                <img alt="icon" src={localAnd} className={plaformClassA} />
                                                <img alt="icon" src={localIOS} className={plaformClassI} />
                                                <img alt="icon" src={localWeb} className={plaformClassW} />
                                                {/* <p>{obj.PLATFORM}</p> */}
                                            </div>
                                        </div>
                                        <div className="col-md-3 col-md-custom-3 no-gutters">
                                            <div className={pmClass}>
                                                {/* <p className='p-padding3vh'>
                                                    {
                                                        obj.COMMENTS.length > 38 ?
                                                            <marquee direction="left" behavior="scroll" scrollamount="4" >
                                                                {obj.COMMENTS}
                                                            </marquee> :
                                                            obj.COMMENTS
                                                    }
                                                </p> */}
                                                {
                                                    obj.COMMENTS.length > 38 ?
                                                        <div class="marquee">
                                                            <p>{obj.COMMENTS}</p>
                                                        </div> :
                                                        <p className='p-padding3vh'>{obj.COMMENTS}</p>
                                                }
                                            </div>
                                        </div>
                                    </div>)
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default SalesBarComponent;
