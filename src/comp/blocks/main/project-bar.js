import React, { Component } from 'react';
import { Fade } from 'react-slideshow-image';
var classNames = require('classnames');
const $ = window.$;
const properties = {
    duration: 5000,
    transitionDuration: 1000,
    infinite: true,
    arrows: false,
    onChange: (oldIndex, newIndex) => {
    }
}
const moment = require('moment');

function isOdd(n) {
    return Math.abs(n % 2) === 1;
}

class ProjectBarComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            users: [],
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.data,
            users: this.props.users
        });
        this.roundRobinScrollInit();
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps) !== JSON.stringify(this.props)) {
            this.setState({
                data: nextProps.data,
                users: nextProps.users
            });
        }
    }

    getPmMemberObj(team, userArry) {
        let teamObj = {};
        if (team) {
            let teamMen = userArry.filter((o) => Number(o.USER_ID) === Number(team));
            if (teamMen.length) {
                teamObj = teamMen[0];
            }
            return teamObj;
        } else {
            return null;
        }
    }

    getTeamMemberObj(team, userArry) {
        if (team) {
            let teamArr = team.split(',');
            let teamObj = [];
            if (teamArr.length) {
                for (let i = 0; i < teamArr.length; i++) {
                    let teamMen = userArry.filter((o) => Number(o.USER_ID) === Number(teamArr[i]));
                    if (teamMen.length) {
                        teamObj.push(teamMen[0])
                    }
                }
                return teamObj;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    spiltArray(a) {
        var arrays = [], size = 4;
        while (a.length > 0)
            arrays.push(a.splice(0, size));
        return arrays;
    }

    fadeSliderUi(teamMemberslist) {
        if (teamMemberslist.length) {
            let splitTeamMember = this.spiltArray(teamMemberslist);
            let memView = splitTeamMember.map((mO) => {
                return (<div>
                    {
                        mO.map((mem) => {
                            let iconColor = classNames({
                                'fa': true,
                                'fa-lock': true,
                                'user-present': mem.Available === 'yes',
                                'user-absent': mem.Available === 'no',
                                'user-wfh': mem.Available === 'wfh',
                            });
                            return (
                                <div className="tmdiv-img">
                                    {
                                        mem.Profile_Image ?
                                            <div>
                                                <img className="profile-img-tm" alt="img" src={this.imgUrlMake(mem.Profile_Image)} />
                                                <i className={iconColor}></i>
                                            </div>
                                            : ''

                                    }

                                </div>
                            )
                        })
                    }
                </div>)
            });
            return memView;
        } else {
            return '';
        }
    }

    roundRobinScrollInit() {
        setTimeout(function () {
            setInterval(function () {
                $('.pb-vertical-inner').first()
                    .animate({ 'margin-top': '-=8.5vh' }, "slow", function () {
                        $(this).appendTo('.pb-vertical-scroll').css('margin-top', '');
                    });
            }, 5000);
        }, 3000)
    }

    imgUrlMake(url) {
        return url;
        // let dummylink = 'https://drive.google.com/open?id=1G20Nb_LhZYfgrcwiEoNXhrRgoWaFTcU5'
        // if (url) {
        //     return url;
        //     // return url.replace('/open', '/uc');
        // } else {
        //     return dummylink.replace('/open', '/uc');;
        // }
    }

    isTimeLineDelayed(timelineTaken, timelineGiven) {
        let tlT = Number(timelineTaken);
        let tlG = Number(timelineGiven);
        if (tlT > tlG) {
            return true;
        } else {
            return false;
        }
    }

    getTimeline(data) {
        var startDate = moment(data.Date_of_start);
        var curDate = moment();
        var endDate = moment(data.Date_of_submisson);
        var timeGiven = endDate.diff(startDate, 'weeks');
        var timeTaken = curDate.diff(startDate, 'weeks');
        console.log(timeGiven, timeTaken, startDate.format('DD/MM/YYYY'), endDate.format('DD/MM/YYYY'), curDate.format('dd/mm/yyyy'), 'babin');
        return {
            isDelayed: this.isTimeLineDelayed(timeTaken, timeGiven),
            timeGiven: timeGiven,
            timeTaken: timeTaken
        }
    }
    render() {
        const { data, users } = this.state;
        return (
            <div>
                <div className="projectdiv">
                    <div className="row no-gutters">
                        <div className="col-md-3 no-gutters">
                            <div className="heading">
                                <p>PROJECT</p>
                            </div>
                        </div>
                        <div className="col-md-2 no-gutters">
                            <div className=" heading">
                                <p>TIME LINE</p>
                            </div>
                        </div>
                        <div className="col-md-1 no-gutters">
                            <div className="pmlist heading text-nv-center">
                                <p>PM</p>
                            </div>
                        </div>
                        <div className="col-md-3 no-gutters">
                            <div className="teammemberslist heading">
                                <p>TEAM MEMBERS</p>
                            </div>
                        </div>
                        <div className="col-md-3 no-gutters">
                            <div className="commentslist heading">
                                <p>COMMENTS</p>
                            </div>
                        </div>
                    </div>
                    <div className="row no-gutters pb-vertical-scroll">
                        {
                            data.map((obj, i) => {
                                var timeLineData = this.getTimeline(obj);
                                var projectClassMain = classNames({
                                    'projectlist': true,
                                    'project-good-odd': obj.PROJECT_STATUS === 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS === 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS === 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS === 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS === 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS === 'mid' && !isOdd(i)
                                });
                                var projectInner = classNames({
                                    'project-inner-div': true,
                                    'bor-lt1': obj.PROJECT_STATUS === 'good',
                                    'bor-lt2': obj.PROJECT_STATUS === 'bad',
                                    'bor-lt3': obj.PROJECT_STATUS === 'mid',
                                });
                                var timeLineDivClass = classNames({
                                    'timelinelist': true,
                                    'project-good-odd': obj.PROJECT_STATUS === 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS === 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS === 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS === 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS === 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS === 'mid' && !isOdd(i)
                                });
                                var timelineClass = classNames({
                                    'timelinetext': true,
                                    'tl-ontime': !timeLineData.isDelayed,
                                    'tl-delayed': timeLineData.isDelayed,
                                    'p-padding3vh': true
                                })
                                var pmClass = classNames({
                                    'pmimg': true,
                                    'project-good-odd': obj.PROJECT_STATUS === 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS === 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS === 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS === 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS === 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS === 'mid' && !isOdd(i)
                                });
                                var tmClass = classNames({
                                    'tmdiv': true,
                                    'marquee-div': true,
                                    'project-good-odd': obj.PROJECT_STATUS === 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS === 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS === 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS === 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS === 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS === 'mid' && !isOdd(i)
                                });
                                var comClass = classNames({
                                    'commentlist': true,
                                    'project-good-odd': obj.PROJECT_STATUS === 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS === 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS === 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS === 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS === 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS === 'mid' && !isOdd(i)
                                });
                                var pmInfo = this.getPmMemberObj(obj['PM'], users);
                                var teamMemberslist = this.getTeamMemberObj(obj['TEAM MEMBERS'], users);
                                let pmIconColor = classNames({
                                    'fa': true,
                                    'fa-lock': true,
                                    'user-present': pmInfo ? pmInfo.Available === 'yes' : false,
                                    'user-absent': pmInfo ? pmInfo.Available === 'no' : false,
                                    'user-wfh': pmInfo ? pmInfo.Available === 'wfh' : false
                                });
                                return (
                                    <div className="pb-vertical-inner">
                                        <div className="col-md-3 no-gutters">
                                            <div className={projectClassMain}>
                                                <div className={projectInner}>
                                                    <p>{obj.PROJECT}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-2 no-gutters">
                                            <div className={timeLineDivClass}>
                                                <p className={timelineClass}>{timeLineData.timeGiven && timeLineData.timeTaken ? timeLineData.timeTaken + 'W / ' + timeLineData.timeGiven + 'W' : ''}</p>
                                            </div>
                                        </div>
                                        <div className="col-md-1 no-gutters">
                                            <div className={pmClass+" text-nv-center"}>
                                                {
                                                    pmInfo ?
                                                        <div className="pmimg_img">
                                                            <img className="profile-img" alt="img" src={this.imgUrlMake(pmInfo.Profile_Image)} />
                                                            <i className={pmIconColor}></i>
                                                        </div> : ''
                                                }
                                            </div>
                                        </div>
                                        <div className="col-md-3 no-gutters">
                                            <div className={tmClass}>
                                                <div className="no-marquee">
                                                    {teamMemberslist.length ? <Fade  {...properties}>
                                                        {
                                                            this.fadeSliderUi(teamMemberslist)
                                                        }
                                                    </Fade > : ''}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-3 no-gutters">
                                            <div className={comClass}>
                                                {/* <p className='p-padding3vh'>
                                                    {
                                                        obj.COMMENTS.length > 38 ?
                                                            <marquee direction="left" behavior="scroll" scrollamount="3" >
                                                                {obj.COMMENTS}
                                                            </marquee> :
                                                            obj.COMMENTS
                                                    }
                                                </p> */}
                                                {
                                                    obj.COMMENTS.length > 38 ?
                                                        <div class="marquee">
                                                            <p>{obj.COMMENTS}</p>
                                                        </div> :
                                                        <p className='p-padding3vh'>{obj.COMMENTS}</p>
                                                }
                                            </div>
                                        </div>
                                    </div>)
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default ProjectBarComponent;
