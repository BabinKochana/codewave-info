import React, { Component } from 'react';
var classNames = require('classnames');
const $ = window.$;
class ReadyToBoardBlockSalesComponent extends Component {
    constructor() {
        super();
        this.state = {
            users: [],
            project: [],
            rtb: []
        }
    }

    componentDidMount() {
        this.setState({
            users: this.props.users,
            project: this.props.project,
            rtb: []
        });
        this.roundRobinScrollInit();
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps) !== JSON.stringify(this.props)) {
            this.setState({
                users: nextProps.users,
                project: nextProps.project
            }, function () {
                this.getReadyToBoard()
            });
        }
    }

    getReadyToBoard() {
        let member = [];
        let allMen = [];
        let nonAct = [];
        if (this.state.project.length) {
            for (let i = 0; i < this.state.project.length; i++) {
                let teamMem = this.state.project[i]['TEAM MEMBERS'].split(',');
                let pmMem = this.state.project[i]['PM'].split(',');
                member = member.concat(pmMem)
                member = member.concat(teamMem)
            }
            allMen = this.state.users;
        };
        member = member.map(o => o.trim());
        member = member.filter((value, index, self) => {
            return self.indexOf(value) === index;
        })
        for (let i = 0; i < allMen.length; i++) {
            if (member.indexOf(allMen[i].USER_ID) === -1) {
                nonAct.push(allMen[i])
            }
        }
        this.setState({
            rtb: nonAct
        });
    }

    roundRobinScrollInit() {
        setTimeout(function () {
            setInterval(function () {
                $('.rtb-vertical-inner-sales').first()
                    .animate({ 'margin-top': '-=28.5vh' }, 500, function () {
                        $(this).appendTo('.rtb-vertical-scroll-sales').css('margin-top', '');
                    });
            }, 8000);
        }, 3000)
    }

    imgUrlMake(url) {
        //return url.replace('/open', '/uc');
        return url;
    }

    render() {
        var { rtb } = this.state;
        var designArr = rtb.length ? rtb.filter(o => o.Area === "Design") : [];
        var devWebArr = rtb.length ? rtb.filter(o => o.Area === "Web") : [];
        var devMobArr = rtb.length ? rtb.filter(o => o.Area === "Mobile") : [];
        var devBackArr = rtb.length ? rtb.filter(o => o.Area === "Backend") : [];
        var devQaArr = rtb.length ? rtb.filter(o => o.Area === "QA") : [];
        return (
            <div>
                <div className="readytoboard">
                    <div className="readytoboardlist">
                        <p>READY TO BOARD</p>

                    </div>
                    <div className="rtb-vertical-scroll-sales">
                        <div className="rtb-vertical-inner-sales">
                            <div className="rtb-design project-bg1 design-rtb-top">
                                <p className="disigntext">Design</p>
                                <div className="rtobdiv">
                                    {
                                        designArr.map((o) => {
                                            let iconColor = classNames({
                                                'fa': true,
                                                'fa-lock': true,
                                                'user-present': o.Available === 'yes',
                                                'user-absent': o.Available === 'no',
                                                'user-wfh': o.Available === 'wfh',
                                            });
                                            return (
                                                <div className="profile-img-rtb-main">
                                                    <img className="profile-img-rtb" alt="img" src={this.imgUrlMake(o.Profile_Image)} />
                                                    <i className={iconColor}></i>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="rtb-vertical-inner-sales">
                            <div className="rtb-design project-bg2">
                                <p className="disigntext">Dev - Web </p>
                                <div className="rtobdiv">
                                    {
                                        devWebArr.map((o) => {
                                            let iconColor = classNames({
                                                'fa': true,
                                                'fa-lock': true,
                                                'user-present': o.Available === 'yes',
                                                'user-absent': o.Available === 'no',
                                                'user-wfh': o.Available === 'wfh',
                                            });
                                            return (
                                                <div className="profile-img-rtb-main">
                                                    <img className="profile-img-rtb" alt="img" src={this.imgUrlMake(o.Profile_Image)} />
                                                    <i className={iconColor}></i>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="rtb-vertical-inner-sales">
                            <div className="rtb-design project-bg1">
                                <p className="disigntext">Dev - Mobile</p>
                                <div className="rtobdiv">
                                    {
                                        devMobArr.map((o) => {
                                            let iconColor = classNames({
                                                'fa': true,
                                                'fa-lock': true,
                                                'user-present': o.Available === 'yes',
                                                'user-absent': o.Available === 'no',
                                                'user-wfh': o.Available === 'wfh',
                                            });
                                            return (
                                                <div className="profile-img-rtb-main">
                                                    <img className="profile-img-rtb" alt="img" src={this.imgUrlMake(o.Profile_Image)} />
                                                    <i className={iconColor}></i>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="rtb-vertical-inner-sales">
                            <div className="rtb-design project-bg2">
                                <p className="disigntext">Dev - Backend</p>
                                <div className="rtobdiv">
                                    {
                                        devBackArr.map((o) => {
                                            let iconColor = classNames({
                                                'fa': true,
                                                'fa-lock': true,
                                                'user-present': o.Available === 'yes',
                                                'user-absent': o.Available === 'no',
                                                'user-wfh': o.Available === 'wfh',
                                            });
                                            return (
                                                <div className="profile-img-rtb-main">
                                                    <img className="profile-img-rtb" alt="img" src={this.imgUrlMake(o.Profile_Image)} />
                                                    <i className={iconColor}></i>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="rtb-vertical-inner-sales">
                            <div className="rtb-design project-bg1">
                                <p className="disigntext">QA</p>
                                <div className="rtobdiv">
                                    {
                                        devQaArr.map((o) => {
                                            let iconColor = classNames({
                                                'fa': true,
                                                'fa-lock': true,
                                                'user-present': o.Available === 'yes',
                                                'user-absent': o.Available === 'no',
                                                'user-wfh': o.Available === 'wfh',
                                            });
                                            return (
                                                <div className="profile-img-rtb-main">
                                                    <img className="profile-img-rtb" alt="img" src={this.imgUrlMake(o.Profile_Image)} />
                                                    <i className={iconColor}></i>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ReadyToBoardBlockSalesComponent;
