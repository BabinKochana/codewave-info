import React, { Component } from 'react';
import Marquee from 'react-double-marquee';
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import { Fade } from 'react-slideshow-image';
var classNames = require('classnames');
let localImg = require('../../assets/img/download2.png');
const properties = {
    duration: 5000,
    transitionDuration: 1000,
    infinite: true,
    arrows: false,
    onChange: (oldIndex, newIndex) => {
        console.log(`slide transition from ${oldIndex} to ${newIndex}`);
    }
}
function isEven(n) {
    return n % 2 == 0;
}

function isOdd(n) {
    return Math.abs(n % 2) == 1;
}
class TeamMemberBlockComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            column: [],
            users: [],
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.data,
            users: this.props.users
        });
        this.formatColumn(this.props.data)
    }


    formatColumn(array) {
        if (array.length) {
            let firstElem = array[0];
            let keysArr = Object.keys(firstElem);
            let formatedColumn = keysArr.map(obj => ({
                Header: obj,
                accessor: obj
            }));
            this.setState({
                column: formatedColumn,
            })

        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props) {
            this.setState({
                data: nextProps.data,
                users: nextProps.users
            });
            this.formatColumn(nextProps.data)
        }
    }

    spiltArray(a) {
        var arrays = [], size = 5;
        while (a.length > 0)
            arrays.push(a.splice(0, size));
        return arrays;
    }


    getTeamMemberObj(team, userArry) {
        if (team) {
            let teamArr = team.split(',');
            let teamObj = [];
            if (teamArr.length) {
                teamArr.map((obj) => {
                    let teamMen = userArry.filter((o) => o.USER_ID == obj);
                    if (teamMen.length) {
                        teamObj.push(teamMen[0])
                    }
                })
                return teamObj;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    fadeSliderUi(teamMemberslist) {
        if (teamMemberslist.length) {
            let splitTeamMember = this.spiltArray(teamMemberslist);
            let memView = splitTeamMember.map((mO) => {
                return (<div>
                    {
                        mO.map((mem) => {
                            let iconColor = classNames({
                                'fa': true,
                                'fa-lock': true,
                                'user-present': mem.Available == 'yes',
                                'user-absent': mem.Available == 'no'
                            });
                            return (
                                <div class="tmdiv-img">
                                    <img className="profile-img-tm" alt="img" src={mem.IMAGE_LINK} />
                                    <i className={iconColor}></i>
                                </div>
                            )
                        })
                    }
                </div>)
            });
            return memView;
        } else {
            return '';
        }
    }
    render() {
        const { data, column, users } = this.state;
        return (
            <div>
                <div class="teammembers">
                    <div class="teammemberslist heading">
                        <p>TEAM MEMBERS</p>
                    </div>
                    {
                        data.map((obj, i) => {
                            var blockClass = classNames({
                                'tmdiv': true,
                                'marquee-div': true,
                                'project-good-odd': obj.PROJECT_STATUS == 'good' && isOdd(i),
                                'project-good-even': obj.PROJECT_STATUS == 'good' && !isOdd(i),
                                'project-bad-odd': obj.PROJECT_STATUS == 'bad' && isOdd(i),
                                'project-bad-even': obj.PROJECT_STATUS == 'bad' && !isOdd(i),
                                'project-mid-odd': obj.PROJECT_STATUS == 'mid' && isOdd(i),
                                'project-mid-even': obj.PROJECT_STATUS == 'mid' && !isOdd(i)
                            });
                            var teamMemberslist = this.getTeamMemberObj(obj['TEAM MEMBERS'], users);
                            return (
                                <div className={blockClass}>
                                    <div className="no-marquee">
                                        {/* {
                                            this.fadeSliderUi(teamMemberslist)
                                        } */}
                                        <Fade  {...properties}>
                                            {
                                                this.fadeSliderUi(teamMemberslist)
                                            }
                                        </Fade >
                                    </div>
                                </div>)
                        })
                    }
                </div>
            </div>
        );
    }
}

export default TeamMemberBlockComponent;
