import React, { Component } from 'react';
var classNames = require('classnames');
function isEven(n) {
    return n % 2 == 0;
}

function isOdd(n) {
    return Math.abs(n % 2) == 1;
}
class TimelineBlockComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            column: []
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.data
        });
        this.formatColumn(this.props.data)
    }


    formatColumn(array) {
        if (array.length) {
            let firstElem = array[0];
            let keysArr = Object.keys(firstElem);
            let formatedColumn = keysArr.map(obj => ({
                Header: obj,
                accessor: obj
            }));
            this.setState({
                column: formatedColumn,
            })

        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props) {
            this.setState({
                data: nextProps.data
            });
            this.formatColumn(nextProps.data)
        }
    }

    render() {
        const { data, column } = this.state;
        return (
            <div>
                <div class="timeline ">
                    <div class=" heading">
                        <p>TIME LINE</p>
                    </div>
                    {
                        data.map((obj, i) => {
                            var blockClass = classNames({
                                'timelinelist': true,
                                'project-good-odd': obj.PROJECT_STATUS == 'good' && isOdd(i),
                                'project-good-even': obj.PROJECT_STATUS == 'good' && !isOdd(i),
                                'project-bad-odd': obj.PROJECT_STATUS == 'bad' && isOdd(i),
                                'project-bad-even': obj.PROJECT_STATUS == 'bad' && !isOdd(i),
                                'project-mid-odd': obj.PROJECT_STATUS == 'mid' && isOdd(i),
                                'project-mid-even': obj.PROJECT_STATUS == 'mid' && !isOdd(i)
                            });
                            var timelineClass = classNames({
                                'timelinetext': true,
                                'tl-ontime': obj.PROJECT_DELAYED == 'no',
                                'tl-delayed': obj.PROJECT_DELAYED == 'yes',
                                'tl-mid': obj.PROJECT_DELAYED == 'mid'
                            })
                            return (
                                <div className={blockClass}>
                                    <p className={timelineClass}>{obj['TIME LINE']}</p>
                                </div>)
                        })
                    }
                </div>
            </div>
        );
    }
}

export default TimelineBlockComponent;
