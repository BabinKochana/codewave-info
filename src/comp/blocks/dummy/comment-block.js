import React, { Component } from 'react';
var classNames = require('classnames');

function isEven(n) {
    return n % 2 == 0;
}

function isOdd(n) {
    return Math.abs(n % 2) == 1;
}
class CommentBlockComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            column: []
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.data
        });
        this.formatColumn(this.props.data)
    }


    formatColumn(array) {
        if (array.length) {
            let firstElem = array[0];
            let keysArr = Object.keys(firstElem);
            let formatedColumn = keysArr.map(obj => ({
                Header: obj,
                accessor: obj
            }));
            this.setState({
                column: formatedColumn,
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props) {
            this.setState({
                data: nextProps.data
            });
            this.formatColumn(nextProps.data)
        }
    }

    render() {
        const { data, column } = this.state;
        return (
            <div>
                <div class="comments">
                    <div class="commentslist heading">
                        <p>COMMENTS</p>

                    </div>
                    {
                        data.map((obj, i) => {
                            var blockClass = classNames({
                                'commentlist': true,
                                'project-good-odd': obj.PROJECT_STATUS == 'good' && isOdd(i),
                                'project-good-even': obj.PROJECT_STATUS == 'good' && !isOdd(i),
                                'project-bad-odd': obj.PROJECT_STATUS == 'bad' && isOdd(i),
                                'project-bad-even': obj.PROJECT_STATUS == 'bad' && !isOdd(i),
                                'project-mid-odd': obj.PROJECT_STATUS == 'mid' && isOdd(i),
                                'project-mid-even': obj.PROJECT_STATUS == 'mid' && !isOdd(i)
                            });
                            return (<div className={blockClass}>
                                <p>
                                    {
                                        obj.COMMENTS.length > 42 ?
                                            <marquee direction="left" behavior="scroll" > 
                                                {obj.COMMENTS}
                                            </marquee> :
                                            obj.COMMENTS
                                    }
                                </p>

                            </div>)
                        })
                    }
                </div>
            </div>
        );
    }
}

export default CommentBlockComponent;
