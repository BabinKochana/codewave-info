import React, { Component } from 'react';
var classNames = require('classnames');
let localImg = require('../../assets/img/download2.png');
function isEven(n) {
    return n % 2 == 0;
}

function isOdd(n) {
    return Math.abs(n % 2) == 1;
}
class ProjectManagerBlockComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            column: [],
            users: [],
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.data,
            users: this.props.users
        });
        this.formatColumn(this.props.data)
    }


    formatColumn(array) {
        if (array.length) {
            let firstElem = array[0];
            let keysArr = Object.keys(firstElem);
            let formatedColumn = keysArr.map(obj => ({
                Header: obj,
                accessor: obj
            }));
            this.setState({
                column: formatedColumn,
            })

        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props) {
            this.setState({
                data: nextProps.data,
                users: nextProps.users
            });
            this.formatColumn(nextProps.data)
        }
    }

    getTeamMemberObj(team, userArry) {
        let teamObj = {};
        if (team) {
            let teamMen = userArry.filter((o) => o.USER_ID == team);
            if (teamMen.length) {
                teamObj = teamMen[0];
            }
            return teamObj;
        } else {
            return null;
        }
    }

    render() {
        const { data, column, users } = this.state;
        return (
            <div>
                <div class="pmcol">
                    <div class="pmlist heading">
                        <p>PM</p>
                    </div>
                    <div class="pmlist">
                        {
                            data.map((obj, i) => {
                                var blockClass = classNames({
                                    'pmimg': true,
                                    'project-good-odd': obj.PROJECT_STATUS == 'good' && isOdd(i),
                                    'project-good-even': obj.PROJECT_STATUS == 'good' && !isOdd(i),
                                    'project-bad-odd': obj.PROJECT_STATUS == 'bad' && isOdd(i),
                                    'project-bad-even': obj.PROJECT_STATUS == 'bad' && !isOdd(i),
                                    'project-mid-odd': obj.PROJECT_STATUS == 'mid' && isOdd(i),
                                    'project-mid-even': obj.PROJECT_STATUS == 'mid' && !isOdd(i)
                                });
                                var pmInfo = this.getTeamMemberObj(obj['PM'], users);
                                return (<div className={blockClass}>
                                    <img className="profile-img" alt="img" src={pmInfo ? pmInfo.IMAGE_LINK : ''} />
                                </div>)
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default ProjectManagerBlockComponent;
