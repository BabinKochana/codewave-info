import React, { Component } from 'react';
import { Carousel } from 'antd';
var classNames = require('classnames');

function isEven(n) {
    return n % 2 == 0;
}

function isOdd(n) {
    return Math.abs(n % 2) == 1;
}
class ProjectBlockComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            column: []
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.data
        });
        this.formatColumn(this.props.data)
    }


    formatColumn(array) {
        if (array.length) {
            let firstElem = array[0];
            let keysArr = Object.keys(firstElem);
            let formatedColumn = keysArr.map(obj => ({
                Header: obj,
                accessor: obj
            }));
            this.setState({
                column: formatedColumn,
            })

        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props) {
            this.setState({
                data: nextProps.data
            });
            this.formatColumn(nextProps.data)
        }
    }


    render() {
        const { data, column } = this.state;
        return (
            <div>
                <div class="projectdiv">
                    <div class="heading">
                        <p>PROJECT</p>
                    </div>
                    <div className="project-vertical-scroll">
                        <Carousel vertical autoplay={true} slidesToShow={10} dots={false} infinite={true} >
                            {
                                data.map((obj, i) => {
                                    var blockClass = classNames({
                                        'projectlist': true,
                                        'project-good-odd': obj.PROJECT_STATUS == 'good' && isOdd(i),
                                        'project-good-even': obj.PROJECT_STATUS == 'good' && !isOdd(i),
                                        'project-bad-odd': obj.PROJECT_STATUS == 'bad' && isOdd(i),
                                        'project-bad-even': obj.PROJECT_STATUS == 'bad' && !isOdd(i),
                                        'project-mid-odd': obj.PROJECT_STATUS == 'mid' && isOdd(i),
                                        'project-mid-even': obj.PROJECT_STATUS == 'mid' && !isOdd(i),
                                        'bor-lt1': obj.PROJECT_STATUS == 'good',
                                        'bor-lt2': obj.PROJECT_STATUS == 'bad',
                                        'bor-lt3': obj.PROJECT_STATUS == 'mid',
                                    });
                                    return (
                                        <div>
                                            <div className={blockClass}>
                                                <p>{obj.PROJECT}</p>
                                            </div>
                                        </div>)
                                })
                            }
                        </Carousel>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProjectBlockComponent;
