import React, { Component } from 'react';
import ReadyToBoardBlockComponent from './blocks/main/ready-to-board-block';
import ProjectBarComponent from './blocks/main/project-bar';
class FirstComponent extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      column: [],
      users: []
    }
  }

  componentDidMount() {
    this.setState({
      data: this.props.data,
      users: this.props.users
    });
  }

  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps) !== JSON.stringify(this.props)) {
      this.setState({
        data: nextProps.data,
        users: nextProps.users
      });
    }
  }

  render() {
    const { data, users } = this.state;
    return (
      <div className="maindiv">
        <div className="subdiv">
          <div className="row no-gutters">
            <div className="col-md-10 no-gutters">
              <ProjectBarComponent data={data} users={users} />
            </div>
            <div className="col-md-2 no-gutters">
              <ReadyToBoardBlockComponent users={users} project={data} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FirstComponent;
