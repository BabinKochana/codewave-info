import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// class Demo extends React.Component {
//     render() {
//         return (
//             <div>
//                 <h1 className="title">React Page Scroller Demo</h1>
//                 <div className="links">
//                     <div className="link">
//                         <Link to="/fullpage">Full page demo</Link>
//                     </div>
//                 </div>
//             </div>
//         );
//     }
// }

ReactDOM.render(
    <Router basename="/demos">
        <div>
            <Switch>
                <Route exact path="/" component={App} />
            </Switch>
        </div>
    </Router>,
    document.getElementById("root"),
);
registerServiceWorker();
